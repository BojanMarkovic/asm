graph [
  node [
    id 0
    label "105583"
    name "Dusan"
    surname "Lajovic"
    country "SRB"
    hand "R"
    dateOfBirth 19900630.0
    tourneyNum 15
    avgRank 25.185185185185187
    avgPoints 1700.037037037037
  ]
  node [
    id 1
    label "206173"
    name "Jannik"
    surname "Sinner"
    country "ITA"
    hand "U"
    dateOfBirth 20010816.0
    tourneyNum 12
    avgRank 59.81481481481482
    avgPoints 998.7037037037037
  ]
  node [
    id 2
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 9
    avgRank 1.1111111111111112
    avgPoints 10940.185185185184
  ]
  node [
    id 3
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 11
    avgRank 21.074074074074073
    avgPoints 1987.2222222222222
  ]
  node [
    id 4
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 13
    avgRank 33.074074074074076
    avgPoints 1410.3703703703704
  ]
  node [
    id 5
    label "200175"
    name "Miomir"
    surname "Kecmanovic"
    country "SRB"
    hand "R"
    dateOfBirth 19990831.0
    tourneyNum 13
    avgRank 46.22222222222222
    avgPoints 1152.888888888889
  ]
  node [
    id 6
    label "200624"
    name "Sebastian"
    surname "Korda"
    country "USA"
    hand "U"
    dateOfBirth 20000705.0
    tourneyNum 3
    avgRank 180.74074074074073
    avgPoints 355.74074074074076
  ]
  node [
    id 7
    label "111456"
    name "Mackenzie"
    surname "Mcdonald"
    country "USA"
    hand "R"
    dateOfBirth 19950416.0
    tourneyNum 8
    avgRank 202.37037037037038
    avgPoints 290.0740740740741
  ]
  node [
    id 8
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 9
    avgRank 24.74074074074074
    avgPoints 1689.6296296296296
  ]
  node [
    id 9
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 12
    avgRank 55.888888888888886
    avgPoints 985.7777777777778
  ]
  node [
    id 10
    label "104655"
    name "Pablo"
    surname "Cuevas"
    country "URU"
    hand "R"
    dateOfBirth 19860101.0
    tourneyNum 12
    avgRank 60.111111111111114
    avgPoints 930.3333333333334
  ]
  node [
    id 11
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 14
    avgRank 11.88888888888889
    avgPoints 2926.222222222222
  ]
  node [
    id 12
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 8
    avgRank 3.3333333333333335
    avgPoints 8073.148148148148
  ]
  node [
    id 13
    label "126952"
    name "Soon Woo"
    surname "Kwon"
    country "KOR"
    hand "R"
    dateOfBirth 19971202.0
    tourneyNum 7
    avgRank 85.22222222222223
    avgPoints 727.0
  ]
  node [
    id 14
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 10
    avgRank 19.77777777777778
    avgPoints 2000.3333333333333
  ]
  node [
    id 15
    label "106078"
    name "Egor"
    surname "Gerasimov"
    country "BLR"
    hand "R"
    dateOfBirth 19921111.0
    tourneyNum 10
    avgRank 79.44444444444444
    avgPoints 756.3333333333334
  ]
  node [
    id 16
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 10
    avgRank 7.0
    avgPoints 4496.666666666667
  ]
  node [
    id 17
    label "103852"
    name "Feliciano"
    surname "Lopez"
    country "ESP"
    hand "L"
    dateOfBirth 19810920.0
    tourneyNum 10
    avgRank 59.333333333333336
    avgPoints 951.7037037037037
  ]
  node [
    id 18
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 7
    avgRank 1.8888888888888888
    avgPoints 9816.111111111111
  ]
  node [
    id 19
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 3
    avgRank 37.925925925925924
    avgPoints 1279.8148148148148
  ]
  node [
    id 20
    label "106198"
    name "Hugo"
    surname "Dellien"
    country "BOL"
    hand "R"
    dateOfBirth 19930616.0
    tourneyNum 8
    avgRank 97.74074074074075
    avgPoints 660.7777777777778
  ]
  node [
    id 21
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 12
    avgRank 79.55555555555556
    avgPoints 761.5185185185185
  ]
  node [
    id 22
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 10
    avgRank 12.407407407407407
    avgPoints 2550.185185185185
  ]
  node [
    id 23
    label "105882"
    name "Stefano"
    surname "Travaglia"
    country "ITA"
    hand "U"
    dateOfBirth 19911218.0
    tourneyNum 10
    avgRank 78.18518518518519
    avgPoints 771.8518518518518
  ]
  node [
    id 24
    label "104665"
    name "Pablo"
    surname "Andujar"
    country "ESP"
    hand "R"
    dateOfBirth 19860123.0
    tourneyNum 12
    avgRank 57.44444444444444
    avgPoints 968.8888888888889
  ]
  node [
    id 25
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 12
    avgRank 11.444444444444445
    avgPoints 2764.074074074074
  ]
  node [
    id 26
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 14
    avgRank 5.888888888888889
    avgPoints 5405.185185185185
  ]
  node [
    id 27
    label "111442"
    name "Jordan"
    surname "Thompson"
    country "AUS"
    hand "R"
    dateOfBirth 19940420.0
    tourneyNum 13
    avgRank 58.44444444444444
    avgPoints 943.6666666666666
  ]
  node [
    id 28
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 15
    avgRank 29.48148148148148
    avgPoints 1537.2222222222222
  ]
  node [
    id 29
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 12
    avgRank 4.7407407407407405
    avgPoints 6547.592592592592
  ]
  edge [
    source 0
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 3
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 15
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 3
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 22
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 16
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 8
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 18
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 25
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 2
    target 12
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 2
    target 3
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 2
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 18
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 3
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 13
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 11
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 11
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 27
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 9
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 16
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 10
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 26
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 11
    target 12
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 18
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 29
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 26
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 15
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 26
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 16
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 29
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 24
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 22
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 20
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 25
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 18
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 27
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 26
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 29
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 21
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 23
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 24
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 29
    weight 3
    lowerId 0
    higherId 3
  ]
]
