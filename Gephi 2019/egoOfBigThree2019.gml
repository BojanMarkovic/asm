graph [
  node [
    id 0
    label "106045"
    name "Denis"
    surname "Kudla"
    country "USA"
    hand "R"
    dateOfBirth 19920817.0
    tourneyNum 22
    avgRank 93.46808510638297
    avgPoints 608.3829787234042
  ]
  node [
    id 1
    label "105676"
    name "David"
    surname "Goffin"
    country "BEL"
    hand "R"
    dateOfBirth 19901207.0
    tourneyNum 28
    avgRank 18.595744680851062
    avgPoints 1879.3617021276596
  ]
  node [
    id 2
    label "106148"
    name "Roberto"
    surname "Carballes Baena"
    country "ESP"
    hand "R"
    dateOfBirth 19930323.0
    tourneyNum 20
    avgRank 84.74468085106383
    avgPoints 672.2553191489362
  ]
  node [
    id 3
    label "105223"
    name "Juan Martin"
    surname "Del Potro"
    country "ARG"
    hand "R"
    dateOfBirth 19880923.0
    tourneyNum 5
    avgRank 45.1063829787234
    avgPoints 2415.6382978723404
  ]
  node [
    id 4
    label "105332"
    name "Benoit"
    surname "Paire"
    country "FRA"
    hand "R"
    dateOfBirth 19890508.0
    tourneyNum 31
    avgRank 38.08510638297872
    avgPoints 1230.0212765957447
  ]
  node [
    id 5
    label "100644"
    name "Alexander"
    surname "Zverev"
    country "GER"
    hand "R"
    dateOfBirth 19970420.0
    tourneyNum 25
    avgRank 5.042553191489362
    avgPoints 4747.234042553191
  ]
  node [
    id 6
    label "105453"
    name "Kei"
    surname "Nishikori"
    country "JPN"
    hand "R"
    dateOfBirth 19891229.0
    tourneyNum 15
    avgRank 8.27659574468085
    avgPoints 3509.68085106383
  ]
  node [
    id 7
    label "106421"
    name "Daniil"
    surname "Medvedev"
    country "RUS"
    hand "R"
    dateOfBirth 19960211.0
    tourneyNum 24
    avgRank 9.936170212765957
    avgPoints 3646.1063829787236
  ]
  node [
    id 8
    label "105807"
    name "Pablo"
    surname "Carreno Busta"
    country "ESP"
    hand "R"
    dateOfBirth 19910712.0
    tourneyNum 23
    avgRank 38.06382978723404
    avgPoints 1257.4893617021276
  ]
  node [
    id 9
    label "126774"
    name "Stefanos"
    surname "Tsitsipas"
    country "GRE"
    hand "R"
    dateOfBirth 19980812.0
    tourneyNum 27
    avgRank 7.851063829787234
    avgPoints 3751.063829787234
  ]
  node [
    id 10
    label "105916"
    name "Marton"
    surname "Fucsovics"
    country "HUN"
    hand "R"
    dateOfBirth 19920208.0
    tourneyNum 26
    avgRank 52.829787234042556
    avgPoints 976.468085106383
  ]
  node [
    id 11
    label "104745"
    name "Rafael"
    surname "Nadal"
    country "ESP"
    hand "L"
    dateOfBirth 19860603.0
    tourneyNum 18
    avgRank 1.8085106382978724
    avgPoints 8636.170212765957
  ]
  node [
    id 12
    label "106043"
    name "Diego Sebastian"
    surname "Schwartzman"
    country "ARG"
    hand "R"
    dateOfBirth 19920816.0
    tourneyNum 28
    avgRank 19.893617021276597
    avgPoints 1776.9148936170213
  ]
  node [
    id 13
    label "111575"
    name "Karen"
    surname "Khachanov"
    country "RUS"
    hand "R"
    dateOfBirth 19960521.0
    tourneyNum 31
    avgRank 11.595744680851064
    avgPoints 2647.021276595745
  ]
  node [
    id 14
    label "104527"
    name "Stanislas"
    surname "Wawrinka"
    country "SUI"
    hand "R"
    dateOfBirth 19850328.0
    tourneyNum 20
    avgRank 28.127659574468087
    avgPoints 1520.9574468085107
  ]
  node [
    id 15
    label "104925"
    name "Novak"
    surname "Djokovic"
    country "SRB"
    hand "R"
    dateOfBirth 19870522.0
    tourneyNum 18
    avgRank 1.1914893617021276
    avgPoints 10754.04255319149
  ]
  node [
    id 16
    label "104871"
    name "Jeremy"
    surname "Chardy"
    country "FRA"
    hand "R"
    dateOfBirth 19870212.0
    tourneyNum 27
    avgRank 54.340425531914896
    avgPoints 961.5957446808511
  ]
  node [
    id 17
    label "106228"
    name "Juan Ignacio"
    surname "Londero"
    country "ARG"
    hand "R"
    dateOfBirth 19930815.0
    tourneyNum 22
    avgRank 65.2127659574468
    avgPoints 856.5531914893617
  ]
  node [
    id 18
    label "200615"
    name "Alexei"
    surname "Popyrin"
    country "AUS"
    hand "R"
    dateOfBirth 19990805.0
    tourneyNum 20
    avgRank 106.76595744680851
    avgPoints 536.468085106383
  ]
  node [
    id 19
    label "126203"
    name "Taylor Harry"
    surname "Fritz"
    country "USA"
    hand "R"
    dateOfBirth 19971028.0
    tourneyNum 31
    avgRank 39.04255319148936
    avgPoints 1185.2553191489362
  ]
  node [
    id 20
    label "103819"
    name "Roger"
    surname "Federer"
    country "SUI"
    hand "R"
    dateOfBirth 19810808.0
    tourneyNum 14
    avgRank 3.5531914893617023
    avgPoints 6221.063829787234
  ]
  node [
    id 21
    label "105643"
    name "Federico"
    surname "Delbonis"
    country "ARG"
    hand "L"
    dateOfBirth 19901005.0
    tourneyNum 20
    avgRank 74.63829787234043
    avgPoints 745.9574468085107
  ]
  node [
    id 22
    label "144895"
    name "Corentin"
    surname "Moutet"
    country "FRA"
    hand "L"
    dateOfBirth 19990419.0
    tourneyNum 10
    avgRank 105.48936170212765
    avgPoints 547.8723404255319
  ]
  node [
    id 23
    label "105526"
    name "Jan Lennard"
    surname "Struff"
    country "GER"
    hand "R"
    dateOfBirth 19900425.0
    tourneyNum 30
    avgRank 41.808510638297875
    avgPoints 1121.3829787234042
  ]
  node [
    id 24
    label "104259"
    name "Philipp"
    surname "Kohlschreiber"
    country "GER"
    hand "R"
    dateOfBirth 19831016.0
    tourneyNum 25
    avgRank 59.97872340425532
    avgPoints 887.9148936170212
  ]
  node [
    id 25
    label "105023"
    name "Sam"
    surname "Querrey"
    country "USA"
    hand "R"
    dateOfBirth 19871007.0
    tourneyNum 17
    avgRank 52.51063829787234
    avgPoints 960.7446808510638
  ]
  node [
    id 26
    label "105227"
    name "Marin"
    surname "Cilic"
    country "CRO"
    hand "R"
    dateOfBirth 19880928.0
    tourneyNum 20
    avgRank 20.382978723404257
    avgPoints 2191.595744680851
  ]
  node [
    id 27
    label "105357"
    name "John"
    surname "Millman"
    country "AUS"
    hand "R"
    dateOfBirth 19890614.0
    tourneyNum 29
    avgRank 52.787234042553195
    avgPoints 966.2553191489362
  ]
  node [
    id 28
    label "106378"
    name "Kyle"
    surname "Edmund"
    country "GBR"
    hand "R"
    dateOfBirth 19950108.0
    tourneyNum 25
    avgRank 38.361702127659576
    avgPoints 1283.404255319149
  ]
  node [
    id 29
    label "105138"
    name "Roberto"
    surname "Bautista Agut"
    country "ESP"
    hand "R"
    dateOfBirth 19880414.0
    tourneyNum 26
    avgRank 15.446808510638299
    avgPoints 2115.2127659574467
  ]
  node [
    id 30
    label "128034"
    name "Hubert"
    surname "Hurkacz"
    country "POL"
    hand "R"
    dateOfBirth 19970211.0
    tourneyNum 25
    avgRank 48.680851063829785
    avgPoints 1025.9787234042553
  ]
  node [
    id 31
    label "106298"
    name "Lucas"
    surname "Pouille"
    country "FRA"
    hand "R"
    dateOfBirth 19940223.0
    tourneyNum 21
    avgRank 25.51063829787234
    avgPoints 1468.723404255319
  ]
  node [
    id 32
    label "106000"
    name "Damir"
    surname "Dzumhur"
    country "BIH"
    hand "R"
    dateOfBirth 19920520.0
    tourneyNum 22
    avgRank 75.68085106382979
    avgPoints 738.3829787234042
  ]
  node [
    id 33
    label "106415"
    name "Yoshihito"
    surname "Nishioka"
    country "JPN"
    hand "L"
    dateOfBirth 19950927.0
    tourneyNum 22
    avgRank 69.93617021276596
    avgPoints 784.4042553191489
  ]
  node [
    id 34
    label "126610"
    name "Matteo"
    surname "Berrettini"
    country "ITA"
    hand "R"
    dateOfBirth 19960412.0
    tourneyNum 27
    avgRank 28.404255319148938
    avgPoints 1726.8936170212767
  ]
  node [
    id 35
    label "104545"
    name "John"
    surname "Isner"
    country "USA"
    hand "R"
    dateOfBirth 19850426.0
    tourneyNum 18
    avgRank 13.74468085106383
    avgPoints 2471.063829787234
  ]
  node [
    id 36
    label "106216"
    name "Bjorn"
    surname "Fratangelo"
    country "USA"
    hand "R"
    dateOfBirth 19930719.0
    tourneyNum 6
    avgRank 146.63829787234042
    avgPoints 390.3829787234043
  ]
  node [
    id 37
    label "106071"
    name "Bernard"
    surname "Tomic"
    country "AUS"
    hand "R"
    dateOfBirth 19921021.0
    tourneyNum 15
    avgRank 117.53191489361703
    avgPoints 526.936170212766
  ]
  node [
    id 38
    label "105967"
    name "Henri"
    surname "Laaksonen"
    country "SUI"
    hand "R"
    dateOfBirth 19920331.0
    tourneyNum 10
    avgRank 114.95744680851064
    avgPoints 502.25531914893617
  ]
  node [
    id 39
    label "105777"
    name "Grigor"
    surname "Dimitrov"
    country "BUL"
    hand "R"
    dateOfBirth 19910516.0
    tourneyNum 21
    avgRank 35.1063829787234
    avgPoints 1320.127659574468
  ]
  node [
    id 40
    label "106283"
    name "Mitchell"
    surname "Krueger"
    country "USA"
    hand "R"
    dateOfBirth 19940112.0
    tourneyNum 1
    avgRank 182.04255319148936
    avgPoints 286.4468085106383
  ]
  node [
    id 41
    label "106233"
    name "Dominic"
    surname "Thiem"
    country "AUT"
    hand "R"
    dateOfBirth 19930903.0
    tourneyNum 22
    avgRank 5.1063829787234045
    avgPoints 4782.978723404255
  ]
  node [
    id 42
    label "104542"
    name "Jo Wilfried"
    surname "Tsonga"
    country "FRA"
    hand "R"
    dateOfBirth 19850417.0
    tourneyNum 22
    avgRank 79.65957446808511
    avgPoints 856.0425531914893
  ]
  node [
    id 43
    label "133430"
    name "Denis"
    surname "Shapovalov"
    country "CAN"
    hand "L"
    dateOfBirth 19990415.0
    tourneyNum 32
    avgRank 24.72340425531915
    avgPoints 1574.5744680851064
  ]
  node [
    id 44
    label "106099"
    name "Salvatore"
    surname "Caruso"
    country "ITA"
    hand "R"
    dateOfBirth 19921215.0
    tourneyNum 6
    avgRank 127.12765957446808
    avgPoints 458.3829787234043
  ]
  node [
    id 45
    label "104424"
    name "Go"
    surname "Soeda"
    country "JPN"
    hand "R"
    dateOfBirth 19840905.0
    tourneyNum 1
    avgRank 167.27659574468086
    avgPoints 337.8723404255319
  ]
  node [
    id 46
    label "105932"
    name "Nikoloz"
    surname "Basilashvili"
    country "GEO"
    hand "R"
    dateOfBirth 19920223.0
    tourneyNum 25
    avgRank 20.02127659574468
    avgPoints 1787.872340425532
  ]
  node [
    id 47
    label "200005"
    name "Ugo"
    surname "Humbert"
    country "FRA"
    hand "L"
    dateOfBirth 19980626.0
    tourneyNum 21
    avgRank 64.44680851063829
    avgPoints 847.1489361702128
  ]
  node [
    id 48
    label "105554"
    name "Daniel"
    surname "Evans"
    country "GBR"
    hand "R"
    dateOfBirth 19900523.0
    tourneyNum 22
    avgRank 74.87234042553192
    avgPoints 832.9787234042553
  ]
  node [
    id 49
    label "127339"
    name "Borna"
    surname "Gojo"
    country "CRO"
    hand "U"
    dateOfBirth 19980227.0
    tourneyNum 2
    avgRank 309.02127659574467
    avgPoints 113.82978723404256
  ]
  node [
    id 50
    label "104792"
    name "Gael"
    surname "Monfils"
    country "FRA"
    hand "R"
    dateOfBirth 19860901.0
    tourneyNum 22
    avgRank 16.914893617021278
    avgPoints 2027.340425531915
  ]
  node [
    id 51
    label "200000"
    name "Felix"
    surname "Auger Aliassime"
    country "CAN"
    hand "R"
    dateOfBirth 20000808.0
    tourneyNum 23
    avgRank 36.40425531914894
    avgPoints 1390.595744680851
  ]
  node [
    id 52
    label "106401"
    name "Nick"
    surname "Kyrgios"
    country "AUS"
    hand "R"
    dateOfBirth 19950427.0
    tourneyNum 18
    avgRank 37.638297872340424
    avgPoints 1219.6808510638298
  ]
  node [
    id 53
    label "104919"
    name "Leonardo"
    surname "Mayer"
    country "ARG"
    hand "R"
    dateOfBirth 19870515.0
    tourneyNum 18
    avgRank 73.80851063829788
    avgPoints 745.3617021276596
  ]
  node [
    id 54
    label "106423"
    name "Thanasi"
    surname "Kokkinakis"
    country "AUS"
    hand "R"
    dateOfBirth 19960410.0
    tourneyNum 4
    avgRank 178.4255319148936
    avgPoints 299.0425531914894
  ]
  node [
    id 55
    label "105173"
    name "Adrian"
    surname "Mannarino"
    country "FRA"
    hand "L"
    dateOfBirth 19880629.0
    tourneyNum 27
    avgRank 49.91489361702128
    avgPoints 1003.8510638297872
  ]
  node [
    id 56
    label "111577"
    name "Jared"
    surname "Donaldson"
    country "USA"
    hand "R"
    dateOfBirth 19961009.0
    tourneyNum 2
    avgRank 443.0425531914894
    avgPoints 150.53191489361703
  ]
  node [
    id 57
    label "120424"
    name "Yannick"
    surname "Maden"
    country "GER"
    hand "U"
    dateOfBirth 19891028.0
    tourneyNum 6
    avgRank 117.70212765957447
    avgPoints 482.27659574468083
  ]
  node [
    id 58
    label "105051"
    name "Matthew"
    surname "Ebden"
    country "AUS"
    hand "R"
    dateOfBirth 19871126.0
    tourneyNum 16
    avgRank 118.70212765957447
    avgPoints 612.0851063829788
  ]
  node [
    id 59
    label "105216"
    name "Yuichi"
    surname "Sugita"
    country "JPN"
    hand "R"
    dateOfBirth 19880918.0
    tourneyNum 4
    avgRank 157.40425531914894
    avgPoints 371.1063829787234
  ]
  node [
    id 60
    label "105311"
    name "Joao"
    surname "Sousa"
    country "POR"
    hand "R"
    dateOfBirth 19890330.0
    tourneyNum 30
    avgRank 54.787234042553195
    avgPoints 938.1702127659574
  ]
  node [
    id 61
    label "200282"
    name "Alex"
    surname "De Minaur"
    country "AUS"
    hand "R"
    dateOfBirth 19990217.0
    tourneyNum 26
    avgRank 26.04255319148936
    avgPoints 1475.8936170212767
  ]
  node [
    id 62
    label "104999"
    name "Mischa"
    surname "Zverev"
    country "GER"
    hand "L"
    dateOfBirth 19870822.0
    tourneyNum 12
    avgRank 184.27659574468086
    avgPoints 394.8510638297872
  ]
  node [
    id 63
    label "104926"
    name "Fabio"
    surname "Fognini"
    country "ITA"
    hand "R"
    dateOfBirth 19870524.0
    tourneyNum 25
    avgRank 12.595744680851064
    avgPoints 2402.2340425531916
  ]
  node [
    id 64
    label "105870"
    name "Yannick"
    surname "Hanfmann"
    country "GER"
    hand "U"
    dateOfBirth 19911113.0
    tourneyNum 2
    avgRank 180.80851063829786
    avgPoints 288.8085106382979
  ]
  node [
    id 65
    label "105550"
    name "Guido"
    surname "Pella"
    country "ARG"
    hand "L"
    dateOfBirth 19900517.0
    tourneyNum 29
    avgRank 29.638297872340427
    avgPoints 1412.5531914893618
  ]
  node [
    id 66
    label "126207"
    name "Francis"
    surname "Tiafoe"
    country "USA"
    hand "R"
    dateOfBirth 19980120.0
    tourneyNum 25
    avgRank 40.02127659574468
    avgPoints 1147.872340425532
  ]
  node [
    id 67
    label "111202"
    name "Hyeon"
    surname "Chung"
    country "KOR"
    hand "R"
    dateOfBirth 19960519.0
    tourneyNum 8
    avgRank 121.65957446808511
    avgPoints 526.9574468085107
  ]
  node [
    id 68
    label "103970"
    name "David"
    surname "Ferrer"
    country "ESP"
    hand "R"
    dateOfBirth 19820402.0
    tourneyNum 6
    avgRank 162.47727272727272
    avgPoints 348.52272727272725
  ]
  node [
    id 69
    label "104607"
    name "Tomas"
    surname "Berdych"
    country "CZE"
    hand "R"
    dateOfBirth 19850917.0
    tourneyNum 9
    avgRank 96.4090909090909
    avgPoints 594.2045454545455
  ]
  node [
    id 70
    label "105902"
    name "James"
    surname "Duckworth"
    country "AUS"
    hand "R"
    dateOfBirth 19920121.0
    tourneyNum 3
    avgRank 149.89361702127658
    avgPoints 389.0425531914894
  ]
  node [
    id 71
    label "105936"
    name "Filip"
    surname "Krajinovic"
    country "SRB"
    hand "R"
    dateOfBirth 19920227.0
    tourneyNum 21
    avgRank 63.48936170212766
    avgPoints 887.4255319148937
  ]
  node [
    id 72
    label "104269"
    name "Fernando"
    surname "Verdasco"
    country "ESP"
    hand "L"
    dateOfBirth 19831115.0
    tourneyNum 27
    avgRank 36.87234042553192
    avgPoints 1217.9787234042553
  ]
  node [
    id 73
    label "105430"
    name "Radu"
    surname "Albot"
    country "MDA"
    hand "R"
    dateOfBirth 19891111.0
    tourneyNum 27
    avgRank 50.234042553191486
    avgPoints 1001.2553191489362
  ]
  node [
    id 74
    label "105376"
    name "Peter"
    surname "Gojowczyk"
    country "GER"
    hand "R"
    dateOfBirth 19890715.0
    tourneyNum 21
    avgRank 97.44680851063829
    avgPoints 581.8936170212766
  ]
  node [
    id 75
    label "106214"
    name "Oscar"
    surname "Otte"
    country "GER"
    hand "R"
    dateOfBirth 19930716.0
    tourneyNum 3
    avgRank 154.9787234042553
    avgPoints 352.4468085106383
  ]
  node [
    id 76
    label "126652"
    name "Jay"
    surname "Clarke"
    country "GBR"
    hand "R"
    dateOfBirth 19980727.0
    tourneyNum 3
    avgRank 180.72340425531914
    avgPoints 292.8085106382979
  ]
  node [
    id 77
    label "105732"
    name "Pierre Hugues"
    surname "Herbert"
    country "FRA"
    hand "R"
    dateOfBirth 19910318.0
    tourneyNum 24
    avgRank 50.87234042553192
    avgPoints 979.8936170212766
  ]
  node [
    id 78
    label "144750"
    name "Lloyd George Muirhead"
    surname "Harris"
    country "RSA"
    hand "R"
    dateOfBirth 19970224.0
    tourneyNum 14
    avgRank 97.65957446808511
    avgPoints 583.7234042553191
  ]
  node [
    id 79
    label "104797"
    name "Denis"
    surname "Istomin"
    country "UZB"
    hand "R"
    dateOfBirth 19860907.0
    tourneyNum 12
    avgRank 134.06382978723406
    avgPoints 445.78723404255317
  ]
  node [
    id 80
    label "126094"
    name "Andrey"
    surname "Rublev"
    country "RUS"
    hand "R"
    dateOfBirth 19971020.0
    tourneyNum 24
    avgRank 63.46808510638298
    avgPoints 973.6382978723404
  ]
  node [
    id 81
    label "132283"
    name "Lorenzo"
    surname "Sonego"
    country "ITA"
    hand "R"
    dateOfBirth 19950511.0
    tourneyNum 24
    avgRank 70.57446808510639
    avgPoints 823.4893617021277
  ]
  node [
    id 82
    label "104731"
    name "Kevin"
    surname "Anderson"
    country "RSA"
    hand "R"
    dateOfBirth 19860518.0
    tourneyNum 5
    avgRank 26.361702127659573
    avgPoints 2800.531914893617
  ]
  node [
    id 83
    label "104755"
    name "Richard"
    surname "Gasquet"
    country "FRA"
    hand "R"
    dateOfBirth 19860618.0
    tourneyNum 18
    avgRank 44.808510638297875
    avgPoints 1111.276595744681
  ]
  node [
    id 84
    label "105077"
    name "Albert"
    surname "Ramos"
    country "ESP"
    hand "L"
    dateOfBirth 19880117.0
    tourneyNum 25
    avgRank 67.25531914893617
    avgPoints 847.8936170212766
  ]
  node [
    id 85
    label "106432"
    name "Borna"
    surname "Coric"
    country "CRO"
    hand "R"
    dateOfBirth 19961114.0
    tourneyNum 23
    avgRank 17.04255319148936
    avgPoints 2133.723404255319
  ]
  node [
    id 86
    label "134770"
    name "Casper"
    surname "Ruud"
    country "NOR"
    hand "R"
    dateOfBirth 19981222.0
    tourneyNum 18
    avgRank 73.08510638297872
    avgPoints 807.531914893617
  ]
  node [
    id 87
    label "111576"
    name "Sumit"
    surname "Nagal"
    country "IND"
    hand "R"
    dateOfBirth 19970816.0
    tourneyNum 2
    avgRank 234.82978723404256
    avgPoints 234.10638297872342
  ]
  edge [
    source 0
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 34
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 0
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 2
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 10
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 28
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 0
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 0
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 0
    target 31
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 7
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 1
    target 9
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 1
    target 4
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 10
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 3
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 14
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 1
    target 11
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 20
    weight 9
    lowerId 9
    higherId 0
  ]
  edge [
    source 1
    target 34
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 5
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 1
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 15
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 1
    target 33
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 19
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 8
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 1
    target 43
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 1
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 1
    target 71
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 1
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 1
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 1
    target 65
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 1
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 1
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 1
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 1
    target 83
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 2
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 12
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 6
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 32
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 2
    target 25
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 2
    target 34
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 33
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 3
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 3
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 3
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 14
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 6
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 4
    target 42
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 4
    target 8
    weight 4
    lowerId 3
    higherId 1
  ]
  edge [
    source 4
    target 17
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 4
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 16
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 9
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 7
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 30
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 46
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 4
    target 32
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 4
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 16
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 5
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 23
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 17
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 9
    weight 9
    lowerId 0
    higherId 9
  ]
  edge [
    source 5
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 34
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 5
    target 21
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 27
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 15
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 5
    target 46
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 5
    target 13
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 12
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 7
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 5
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 5
    target 43
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 5
    target 41
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 5
    target 11
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 5
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 5
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 68
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 5
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 5
    target 51
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 5
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 5
    target 80
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 7
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 6
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 39
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 6
    target 15
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 6
    target 8
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 14
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 6
    target 10
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 30
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 6
    target 19
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 6
    target 12
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 6
    target 42
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 6
    target 11
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 6
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 6
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 6
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 6
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 6
    target 77
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 6
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 42
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 7
    target 15
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 7
    target 10
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 16
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 7
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 7
    target 9
    weight 9
    lowerId 6
    higherId 3
  ]
  edge [
    source 7
    target 41
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 7
    target 31
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 7
    target 12
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 36
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 11
    weight 9
    lowerId 9
    higherId 0
  ]
  edge [
    source 7
    target 13
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 28
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 7
    target 23
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 14
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 7
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 7
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 50
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 7
    target 72
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 7
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 7
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 52
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 7
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 7
    target 80
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 7
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 23
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 8
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 8
    target 43
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 8
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 18
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 15
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 8
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 8
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 8
    target 27
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 20
    weight 15
    lowerId 6
    higherId 9
  ]
  edge [
    source 9
    target 11
    weight 12
    lowerId 9
    higherId 3
  ]
  edge [
    source 9
    target 46
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 9
    target 29
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 9
    target 34
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 9
    target 23
    weight 6
    lowerId 4
    higherId 2
  ]
  edge [
    source 9
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 30
    weight 8
    lowerId 6
    higherId 2
  ]
  edge [
    source 9
    target 43
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 9
    target 10
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 15
    weight 9
    lowerId 6
    higherId 3
  ]
  edge [
    source 9
    target 14
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 9
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 41
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 9
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 19
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 50
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 9
    target 51
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 9
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 53
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 9
    target 55
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 9
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 9
    target 71
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 9
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 9
    target 80
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 9
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 27
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 29
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 46
    weight 5
    lowerId 3
    higherId 2
  ]
  edge [
    source 10
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 23
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 14
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 12
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 24
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 13
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 10
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 10
    target 85
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 10
    target 84
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 10
    target 81
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 10
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 15
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 11
    target 20
    weight 9
    lowerId 6
    higherId 3
  ]
  edge [
    source 11
    target 13
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 11
    target 12
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 11
    target 39
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 29
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 41
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 11
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 14
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 11
    target 46
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 17
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 42
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 11
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 27
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 34
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 11
    target 43
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 11
    target 48
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 11
    target 49
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 52
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 70
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 66
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 56
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 11
    target 65
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 11
    target 53
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 11
    target 64
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 59
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 60
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 11
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 11
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 33
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 12
    target 41
    weight 6
    lowerId 2
    higherId 4
  ]
  edge [
    source 12
    target 19
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 34
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 12
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 12
    target 29
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 25
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 12
    target 13
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 12
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 69
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 12
    target 53
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 12
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 12
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 14
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 13
    target 29
    weight 6
    lowerId 4
    higherId 2
  ]
  edge [
    source 13
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 34
    weight 6
    lowerId 0
    higherId 6
  ]
  edge [
    source 13
    target 46
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 24
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 13
    target 41
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 13
    target 23
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 13
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 19
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 13
    target 15
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 38
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 13
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 13
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 13
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 14
    target 29
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 14
    target 43
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 14
    target 20
    weight 9
    lowerId 9
    higherId 0
  ]
  edge [
    source 14
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 32
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 39
    weight 9
    lowerId 9
    higherId 0
  ]
  edge [
    source 14
    target 16
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 15
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 14
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 14
    target 48
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 14
    target 50
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 14
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 14
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 14
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 29
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 15
    target 46
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 43
    weight 12
    lowerId 12
    higherId 0
  ]
  edge [
    source 15
    target 31
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 15
    target 40
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 42
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 15
    target 24
    weight 8
    lowerId 2
    higherId 6
  ]
  edge [
    source 15
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 21
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 19
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 15
    target 41
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 15
    target 26
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 16
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 15
    target 30
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 15
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 20
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 15
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 17
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 27
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 45
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 15
    target 18
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 15
    target 39
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 15
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 15
    target 34
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 15
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 23
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 28
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 16
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 26
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 16
    target 25
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 16
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 16
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 46
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 17
    target 22
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 20
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 34
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 17
    target 25
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 17
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 60
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 17
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 17
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 17
    target 83
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 17
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 41
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 21
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 18
    target 34
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 18
    target 43
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 35
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 20
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 42
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 29
    weight 6
    lowerId 4
    higherId 2
  ]
  edge [
    source 19
    target 37
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 25
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 30
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 19
    target 23
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 19
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 83
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 19
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 19
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 19
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 24
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 41
    weight 9
    lowerId 0
    higherId 9
  ]
  edge [
    source 20
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 28
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 43
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 20
    target 29
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 20
    target 42
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 20
    target 27
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 20
    target 31
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 34
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 20
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 20
    target 48
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 20
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 53
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 60
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 73
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 74
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 75
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 79
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 85
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 20
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 20
    target 87
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 20
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 27
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 21
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 21
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 29
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 22
    target 39
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 22
    target 44
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 27
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 39
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 23
    target 43
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 23
    target 26
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 23
    target 34
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 23
    target 31
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 23
    target 42
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 23
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 45
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 38
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 52
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 58
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 23
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 23
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 24
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 24
    target 31
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 24
    target 28
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 24
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 50
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 77
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 24
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 24
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 35
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 34
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 36
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 41
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 27
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 29
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 25
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 25
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 25
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 29
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 26
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 43
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 39
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 26
    target 27
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 26
    target 35
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 30
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 26
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 60
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 26
    target 67
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 39
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 27
    target 29
    weight 6
    lowerId 6
    higherId 0
  ]
  edge [
    source 27
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 32
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 27
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 27
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 27
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 27
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 28
    target 42
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 28
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 28
    target 73
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 48
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 28
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 28
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 34
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 29
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 33
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 30
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 58
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 29
    target 61
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 29
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 63
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 29
    target 65
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 29
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 29
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 29
    target 83
    weight 4
    lowerId 1
    higherId 3
  ]
  edge [
    source 30
    target 43
    weight 4
    lowerId 4
    higherId 0
  ]
  edge [
    source 30
    target 31
    weight 6
    lowerId 2
    higherId 4
  ]
  edge [
    source 30
    target 41
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 30
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 30
    target 50
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 30
    target 53
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 30
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 30
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 34
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 31
    target 43
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 42
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 33
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 35
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 76
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 85
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 31
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 31
    target 83
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 31
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 39
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 50
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 32
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 32
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 32
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 33
    target 39
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 39
    weight 6
    lowerId 3
    higherId 3
  ]
  edge [
    source 34
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 41
    weight 9
    lowerId 3
    higherId 6
  ]
  edge [
    source 34
    target 42
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 34
    target 43
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 34
    target 50
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 51
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 34
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 53
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 34
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 79
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 34
    target 80
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 34
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 34
    target 83
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 35
    target 37
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 48
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 35
    target 50
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 61
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 35
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 36
    target 47
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 37
    target 42
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 38
    target 43
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 41
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 39
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 48
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 51
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 39
    target 65
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 39
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 39
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 39
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 43
    weight 3
    lowerId 3
    higherId 0
  ]
  edge [
    source 41
    target 46
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 41
    target 42
    weight 3
    lowerId 0
    higherId 3
  ]
  edge [
    source 41
    target 50
    weight 4
    lowerId 0
    higherId 4
  ]
  edge [
    source 41
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 72
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 41
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 41
    target 83
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 41
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 46
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 42
    target 54
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 42
    target 62
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 42
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 42
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 42
    target 77
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 42
    target 80
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 42
    target 81
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 43
    target 47
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 43
    target 48
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 43
    target 50
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 43
    target 51
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 59
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 60
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 43
    target 61
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 43
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 66
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 43
    target 69
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 43
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 43
    target 77
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 43
    target 80
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 48
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 51
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 46
    target 57
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 62
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 46
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 46
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 48
    target 72
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 48
    target 60
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 48
    target 61
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 48
    target 55
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 71
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 48
    target 73
    weight 3
    lowerId 1
    higherId 2
  ]
  edge [
    source 48
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 48
    target 86
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 48
    target 77
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 53
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 50
    target 55
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 73
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 50
    target 84
    weight 4
    lowerId 2
    higherId 2
  ]
  edge [
    source 50
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 50
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 51
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 52
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 51
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 53
    target 63
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 53
    target 65
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 53
    target 60
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 53
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 57
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 60
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 55
    target 63
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 55
    target 61
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 57
    target 66
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 58
    target 72
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 59
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 65
    weight 3
    lowerId 2
    higherId 1
  ]
  edge [
    source 60
    target 66
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 60
    target 67
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 71
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 60
    target 78
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 60
    target 86
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 60
    target 85
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 60
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 74
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 61
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 61
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 62
    target 68
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 62
    target 65
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 63
    target 66
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 63
    target 71
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 68
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 66
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 67
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 69
    target 72
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 69
    target 71
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 71
    target 72
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 71
    target 73
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 71
    target 74
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 77
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 71
    target 79
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 80
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 71
    target 85
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 71
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 72
    target 73
    weight 2
    lowerId 2
    higherId 0
  ]
  edge [
    source 72
    target 77
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 72
    target 84
    weight 2
    lowerId 0
    higherId 2
  ]
  edge [
    source 72
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 73
    target 81
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 78
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 83
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 80
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 74
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 75
    target 81
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 77
    target 85
    weight 2
    lowerId 1
    higherId 1
  ]
  edge [
    source 77
    target 82
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 85
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 78
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 80
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 80
    target 84
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 80
    target 85
    weight 1
    lowerId 0
    higherId 1
  ]
  edge [
    source 81
    target 84
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 83
    target 87
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 84
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
  edge [
    source 85
    target 86
    weight 1
    lowerId 1
    higherId 0
  ]
]
